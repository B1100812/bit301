#Lessons Learned Report

**Prepared by:**

**Date:**


**Project Name:**
	
**Project Sponsor:**
	
**Project Manager:**
	
**Project Dates:**
	
**Final Budget:**

		
---

1.	Did the project meet scope, time, cost and quality goals?





2.	What was the success criteria listed in the project scope statement?




3.	Reflect on whether or not you met the project success criteria.




4.	In terms of managing the project, what were the main lessons your team learned?





5.	Describe one example of what went right on this project.




6.	Describe one example of what went wrong on this project.





7.	What will you do differently on the next project based on your experience working on this project?





