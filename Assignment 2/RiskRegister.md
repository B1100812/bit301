## Risk Register for (Project Name)

| Rank | Risk | Description | Category | Root Cause | Triggers | Potential Responses | Risk Owner | Probability | Impact | Status | 
| ---- | ---- | ---------- | -------- | ---------- | -------- | ----------------- | ---------- | ---------- | ------ | ------ | 
| *Fill in Rank* | *Risk* | *Description* | *Category* | *Root Cause* | *Triggers* | *Potential Responses* | *Risk Owner* | *Probability* | *Impact* | *Status* | 
